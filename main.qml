import QtQuick
import QtQuick.Effects

Window {
    id: window
    width: 500
    height: 300
    visible: true
    title: "Puppy should be blue"

    Image {
        id: theImage
        source: "image-fill-white.svg"
        //source: "image.svg"
        //source: "image.png"
        sourceSize.height: window.height - 20
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
        visible: false
    }
    MultiEffect {
        source: theImage
        anchors.fill: theImage
        colorization: 1.0
        colorizationColor: "blue"
    }
}