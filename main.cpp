#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    engine.addImportPath(":/myapp");
    const QUrl url(u"qrc:/myapp/puppy/main.qml"_qs);
    engine.load(url);

    return app.exec();
}
